package com.springboot.app;

import com.springboot.app.dbaccess.DriverRepository;
import com.springboot.app.dbaccess.StoreRepository;
import com.springboot.app.model.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@SpringBootApplication
@Slf4j
public class SpringbootaApp {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootaApp.class, args);
		log.info("Springboot and kafka application is started successfully.");
	}
}
