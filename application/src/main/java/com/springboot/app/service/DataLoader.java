package com.springboot.app.service;

import com.springboot.app.dbaccess.DriverRepository;
import com.springboot.app.model.Driver;
import com.springboot.app.model.DriverMapContainer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class DataLoader {

    @Autowired
    public DataLoader(DriverRepository driverRepository) {
        List<Driver> databaseDrivers = driverRepository.findAll();

        for (Driver driver : databaseDrivers) {
            DriverMapContainer.getInstance().addDriver(driver);
        }
    }
}
